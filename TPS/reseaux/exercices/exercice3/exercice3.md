## Docker Network Part3

#### Créer un réseau bridge

Créer un nouveau réseau de type bridge nommé `my_bridge`

```bash
docker network create --driver bridge my_bridge
```

Vérifier que le réseau est présent

```bash
docker network ls
```

Démarrer un nouveau conteneur ubuntu nommé ubuntu3 en mode détaché dans le réseau `my_bridge`

```bash
docker container run -d -it --net=my_bridge --name ubuntu3 ubuntu:14.04
```

Démarrer un conteneur ubuntu nommé `ubuntu4`  dans le réseau `my_bridge` avec un process bash

```bash
docker container run -it --net=my_bridge --name ubuntu4 ubuntu:14.04 bash
```

Lancer un ping vers ubuntu3 en utilisant son nom de conteneur

Lancer un ping vers ubuntu1 et ubuntu2 en utilisant leur nom de conteneur
Les conteneurs sont sur un réseau séparé, le ping ne renvoie pas de réponse


Connecter un conteneur à un autre réseau

Connecter le conteneur ubuntu2 au réseau `my_bridge`

```bash
docker network connect my_bridge ubuntu2
```

Vérifier le ping vers ubuntu3 et ubuntu4 depuis le conteneur ubuntu2

```bash
docker exec ubuntu2 ping ubuntu3
docker exec ubuntu2 ping ubuntu4
```

Vérifier le ping vers ubuntu2 et ubuntu4 depuis le conteneur ubuntu3

```bash
docker exec ubuntu3 ping ubuntu2
docker exec ubuntu3 ping ubuntu4
```
















### Docker network drivers ( bridge,host,null )
// connect different containers using bridge , host and null drivers
// use tcpdump container to show the security issue with Host network

### Expose a Docker Container ( Manual and Dynamic Mapping )
// exposing Elasticsearch 

### Create a custom bridge ( custom network configuration )
// specify a defined network range

### Connecting containers ( using RP connect container )
// using a traefik and 2 containers ( whoami demo ?)

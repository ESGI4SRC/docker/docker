## Docker Network Part2

#### Utiliser le bridge par défaut

Supprimer tous les conteneurs avant de poursuivre l'exercice 

```bash
docker container rm -f $(docker container ls -aq)
```


Démarrer un conteneur ubuntu en mode détaché

```bash
docker container run --name=ubuntu1 -d -it ubuntu:14.04
```

Inspecter le réseau bridge et noter l'adresse IP du conteneur

```bash
docker network inspect bridge
```

// use --format to shrink to NetworkSettings


Démarrer un nouveau conteneur nommé ubuntu et accéder au terminal

```bash
docker container run --name=ubuntu2 -it ubuntu:14.04 bash
```

Lancer un ping vers ubuntu1 avec l'IP obtenue précédemment
Lancer un ping vers ubuntu1 avec son nom de conteneur
Noter que le ping ne fonctionne pas

Quitter le conteneur sans le stopper

```bash
CTRL P + Q
```
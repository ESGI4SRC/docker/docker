FROM debian:buster-slim

# Non-interactif (ecran bleu en ligne de commande)
ENV DEBIAN_FRONTEND=noninteractive

# Mise a jour des depots && install apache2, wget, ca-certificates unzip && supprimer le cache && supprimer le cache (-qqy quiet + yes)
RUN apt-get update -qqy && apt-get install apache2 wget ca-certificates unzip --no-install-recommends -qqy && apt-get clean -qqy && rm -rf /var/lib/apt/*

# Ajout du module proxy php-fpm
RUN a2enmod proxy_fcgi

# Copie de la configuration d'apache
COPY ./conf/web/000-default.conf /etc/apache2/sites-available/000-default.conf

# Copie de la configuration d'apache
COPY ./conf/web/envvars /etc/apache2/envvars

# Changement du repertoire de travail
WORKDIR /var/www/html

# Suppression du fichier index.html par defaut
RUN rm index.html

# Recuperation de joomla + extration
RUN wget https://downloads.joomla.org/cms/joomla3/3-9-27/Joomla_3-9-27-Stable-Full_Package.zip ; unzip Joomla_3-9-27-Stable-Full_Package.zip

# Ajout des droits au dossier joomla
RUN chown www-data:www-data -R . ; chmod 700 -R .

# Suppression du fichier zip d'installation
RUN rm Joomla_3-9-27-Stable-Full_Package.zip

# Suppression des paquets pour le téléchargement
RUN apt-get update -qqy && apt-get purge wget unzip ca-certificates -qqy && apt-get autoremove --purge -qqy && apt-get clean -qqy && rm -rf /var/lib/apt/*

# Execution d'apache
CMD mkdir -p /logs/web/ ; chown www-data:www-data -R /logs/web ; chmod 700 -R /logs/web ; /usr/sbin/apache2ctl -D FOREGROUND

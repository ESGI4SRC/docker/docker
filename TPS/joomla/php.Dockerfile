FROM debian:buster-slim

RUN apt-get update && apt-get install -y php7.3-fpm php7.3-mysql php7.3-zip php7.3-mbstring php7.3-xml php7.3-json

WORKDIR /var/www/html

# Copie de la configuration de php
COPY ./conf/php/php.ini /etc/php/7.3/fpm/php.ini

# Copie de la configuration de php
COPY ./conf/php/php-fpm.conf /etc/php/7.3/fpm/php-fpm.conf

# Ajout du dossier logs
RUN mkdir -p /logs/php

RUN service php7.3-fpm start

CMD php-fpm7.3 -F

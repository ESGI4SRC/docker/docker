# Réseaux dans docker

## Création d'un réseau

```bash
docker network create backend-network
```

## Connecter un docker dans un réseau

```bash
docker run -d --name=redis --net=backend-network redis
```

## Un réseau docker embarque un mini serveur DNS

Exemple : on peut ping redis alors qu'il ne figure pas dans /etc/host
Cela fonctionne aussi sur l'hôte

```bash
docker run --net=backend-network alpine ping -c1 redis
```

## Connecter un docker dans un réseau une fois créé

```bash
docker network connect frontend-network redis
```

Cela peut fonctionner avec des alias (et donc on peut ping l'alias)

```bash
docker network connect --alias db frontend-network2 redis
```

## Déconnecter un docker d'un réseau

```bash
docker network disconnect frontend-network redis
```

## Lister les réseau docker existant

```bash
docker network ls
```

## Inspectier un réseau

```bash
docker network inspect frontend-network
```
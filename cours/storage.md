# Storage

## Data Container vs volumes

Data container est un conteneur donc si on le supprime c'est pas persistant
Volume peut etre distant + persistant

```bash
docker volume create 
docker volume ls
docker volume inspect
docker volume rm

docker volume create --name testvolume

docker container run -v nomduvolume:chemin image

docker volume inspect
```

## Creer un conteneur avec un volume

docker run -v /docker/redis-data:/data --name r1 -d redis redis-server --appendonly yes
